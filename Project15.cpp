﻿
#include <iostream>
#include <string_view>
using namespace std;

void FindEvenNumbers(int Limit, bool IsOdd)
{
    for (int i = 0; i < Limit; i++)
    {
        i = i + i % 2 + !IsOdd;
        cout << i << endl;
    }
}

void main()
{
    int N = 10;
    cout << "Even Numbers:\n";
    FindEvenNumbers(N, true);

    cout << "\nOdd Numbers:\n";
    FindEvenNumbers(N, false);
}